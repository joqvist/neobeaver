/* Copyright (c) 2017-2022, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An Item is a parsing rule with a dot at the current symbol position.
 * LR1 rules have an additional lookahead symbol.
 */
public class Item {
  public final Rule rule;
  public final int dot;
  private final Item next;
  public final Set<Symbol> follow = new HashSet<>();

  public Item(Rule rule, int dot, Symbol... follows) {
    this.rule = rule;
    this.dot = dot;
    for (Symbol sym : follows) {
      follow.add(sym);
    }
    if (dot < rule.rhs.size()) {
      next = new Item(rule, dot + 1, follows);
      succ.add(next);
    } else {
      next = null;
    }
  }

  public void addFollow(Symbol sym) {
    follow.add(sym);
  }

  public void addFollows(Collection<Symbol> sym) {
    follow.addAll(sym);
  }

  /**
   * Find the FIRST set of the sentence after the dot in this rule.
   */
  private SymSet lookaheads(Grammar grammar) {
    SymSet set = new SymSet(grammar);
    for (int i = dot + 1; i < rule.rhs.size(); ++i) {
      Symbol x = rule.rhs.get(i);
      set.or(grammar.first(x));
      if (!grammar.nullable(x)) {
        return set;
      }
    }
    set.addAll(follow);
    return set;
  }

  /**
   * Add the item set following this one to the follow set argument.
   *
   * @param map a map of follow sets, indexed by the lookahead symbol
   */
  public void addToFollowSet(Map<Symbol, Tuple<Symbol, ItemSet>> map) {
    if (canAdvance()) {
      Symbol sym = afterDot();
      Item item = advance();
      Tuple<Symbol, ItemSet> set = map.get(sym);
      if (set == null) {
        set = Tuple.of(sym, new ItemSet(-1));
        map.put(sym, set);
      }
      set.second.items.put(item, item);
    }
  }

  public List<Item> succ = new java.util.LinkedList<>();
  private Collection<Item> extension = null;
  public Collection<Item> immediateExtension(Grammar grammar) {
    if (extension != null) {
      return extension;
    }
    extension = new HashSet<Item>();
    if (dot < rule.rhs.size()) {
      Symbol afterDot = rule.rhs.get(dot);
      boolean nullableItem = nullableAfter(dot+1, grammar);
      SymSet lookahead = lookaheads(grammar);
      for (Rule rule : grammar.byLhs.get(afterDot)) {
        Item item = new Item(rule, 0);
        for (Symbol sym : lookahead) {
          item.follow.add(sym);
        }
        extension.add(item);
        if (nullableItem) {
          succ.add(item);
        }
      }
    }
    return extension;
  }


  /**
   * Returns true if this item is nullable.
   */
  public boolean nullable(Grammar grammar) {
    return nullableAfter(0, grammar);
  }

  /**
   * Returns true if the symbols after the given position form a nullable sentence.
   */
  public boolean nullableAfter(int pos, Grammar grammar) {
    for (int i = pos; i < rule.rhs.size(); ++i) {
      Symbol sym = rule.rhs.get(i);
      if (sym.isTerminal() || !grammar.nullable(sym)) {
        return false;
      }
    }
    return true;
  }

  @Override public String toString() {
    StringBuilder buf = new StringBuilder();
    buf.append(rule.lhs + " =");
    for (int i = 0; i < rule.rhs.size(); ++i) {
      if (i == dot) {
        buf.append(" .");
      }
      buf.append(" ").append(rule.rhs.get(i));
    }
    if (dot == rule.rhs.size()) {
      buf.append(" .");
    }
    if (!follow.isEmpty()) {
      boolean first = true;
      ArrayList<String> follows = new ArrayList<>(follow.size());
      for (Symbol sym : follow) {
        follows.add(Util.escape(sym.toString()));
      }
      Collections.sort(follows);
      buf.append(" [");
      buf.append(String.join(", ", follows));
      buf.append("]");
    }
    return buf.toString();
  }

  @Override public int hashCode() {
    return rule.hashCode() ^ dot;
  }

  @Override public boolean equals(Object obj) {
    if (obj instanceof Item) {
      Item other = (Item) obj;
      return dot == other.dot && rule.equals(other.rule);
    }
    return false;
  }

  public boolean canAdvance() {
    return next != null;
  }

  public Symbol afterDot() {
    return rule.rhs.get(dot);
  }

  public Item advance() {
    if (next == null) {
      throw new Error("Cannot advance dot past end of item.");
    }
    return next;
  }


  /**
   * Gives an item identical to this one but without follow symbol.
   */
  public Item baseItem() {
    return this;
  }

  public Collection<Tuple3<ItemSet, Symbol, Action>> reduceActions(Grammar grammar, ItemSet set) {
    Collection<Tuple3<ItemSet, Symbol, Action>> actions = new ArrayList<>();
    for (Symbol sym : follow) {
      actions.add(Tuple.of(set, sym, (Action) new Reduce(rule)));
    }
    return actions;
  }


  public Collection<Symbol> followSyms() {
    return follow;
  }

  /**
   * Returns {@code true} if the this item is related to a conflict on
   * symbol {@code sym}.
   */
  public boolean related(Symbol sym) {
    return dot == rule.rhs.size() && follow.contains(sym);
  }
}
