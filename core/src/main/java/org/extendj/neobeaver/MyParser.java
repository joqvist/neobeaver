/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MyParser {
  public final Grammar grammar;
  public final TransitionTable transitions;
  public final ItemSet renamedGoal;
  public final List<ItemSet> itemSets;

  public MyParser(Grammar grammar, TransitionTable table, ItemSet renamedGoal,
      List<ItemSet> itemSets) {
    this.grammar = grammar;
    this.transitions = table;
    this.renamedGoal = renamedGoal;
    this.itemSets = itemSets;
  }

  public void printTables() {
    grammar.printTables(this);
  }

  public void printGraph() {
    System.out.println("digraph Parser {");
    System.out.println("  node[shape=rectangle];");
    for (ItemSet set : itemSets) {
      set.printGraphNode();
    }
    System.out.println();
    for (ItemSet set : itemSets) {
      for (Map.Entry<Symbol, ItemSet> edge : transitions.map.get(set).entrySet()) {
        Symbol sym = edge.getKey();
        ItemSet next = edge.getValue();
        System.out.format("  S%d -> S%d [label=\"%s\"];%n", set.id(), next.id(),
            Util.escape(sym.toString()));
      }
    }
    System.out.println("}");
  }

  public void printParser() {
    grammar.printParser(System.out, this, false);
  }

  public void printItems() {
    for (ItemSet set : itemSets) {
      System.out.println(set);
    }
  }

  public void printBeaverSpec() throws IOException {
    grammar.printBeaverSpec(System.out, this);
  }

  public void printBeaverTestSpec() throws IOException {
    grammar.printBeaverTestSpec(System.out, this);
  }

  public void checkProblems(Grammar grammar, ProblemHandler problems, Set<Symbol> unused, boolean unreachableError) {
    transitions.checkProblems(grammar, problems, unused, unreachableError);
  }
}
