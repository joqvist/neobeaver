/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ListComponent implements Symbol {
  public final Symbol sym;
  public final List<Rule> rules;
  public final List<Symbol> symbols;

  public static ListComponent build(Symbol sym, SymbolCache cache) {
    Symbol list = cache.list(sym);
    List<Rule> rules = new ArrayList<>();
    rules.add(new Rule(-1,
        list,
        Collections.singletonList(sym),
        String.format("return new List().add(%s);", sym.actionName()),
        Collections.<String>emptyList(),
        "List"));
    rules.add(new Rule(-1,
        list,
        Lists.asList(list, sym, new Symbol[] {}),
        String.format("return %s.add(%s);", list.actionName(), sym.actionName()),
        Collections.<String>emptyList(),
        "List"));
    rules = new ArrayList<>(Grammar.canonicalRules(rules));
    return new ListComponent(list, rules, Collections.singletonList(sym));
  }

  public static ListComponent buildOptional(Symbol sym, SymbolCache cache) {
    Symbol opt = cache.opt(sym);
    Symbol list = cache.list(sym);
    List<Rule> rules = new ArrayList<>();
    rules.add(new Rule(-1,
        opt,
        Collections.<Symbol>emptyList(),
        "return new List();",
        Collections.<String>emptyList(),
        "List"));
    rules.add(new Rule(-1,
        opt,
        Collections.singletonList(list),
        String.format("return %s;", list.actionName()),
        Collections.<String>emptyList(),
        "List"));
    rules.add(new Rule(-1,
        list, Collections.singletonList(sym),
        String.format("return new List().add(%s);", sym.actionName()),
        Collections.<String>emptyList(),
        "List"));
    rules.add(new Rule(-1,
        list, Lists.asList(list, sym, new Symbol[] {}),
        String.format("return %s.add(%s);", list.actionName(), sym.actionName()),
        Collections.<String>emptyList(),
        "List"));
    rules = new ArrayList<>(Grammar.canonicalRules(rules));
    return new ListComponent(list, rules, Collections.singletonList(sym));
  }

  public ListComponent(Symbol sym, List<Rule> rules, List<Symbol> symbols) {
    this.sym = sym;
    this.rules = rules;
    this.symbols = symbols;
  }

  @Override public String actionName() {
    return null;
  }

  @Override public boolean isTerminal() {
    return false;
  }

  @Override public int id() {
    return -1;
  }

  @Override public boolean isNamed() {
    return false;
  }

  @Override public String name() {
    return null;
  }

  @Override public Collection<? extends Rule> extraRules() {
    return rules;
  }

  @Override public String toString() {
    return "<list>";
  }

  @Override public Parser.SourcePosition pos() {
    return new Parser.SourcePosition(0, 0);
  }

  @Override public void setPosition(Parser.SourcePosition position) {

  }
}
