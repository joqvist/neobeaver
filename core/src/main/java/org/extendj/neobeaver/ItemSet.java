/* Copyright (c) 2017-2022, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static se.llbit.util.CollectionComparison.isEqualCollection;

/**
 * Represents a parser state.
 */
public class ItemSet {
  private final int id;
  public final Map<Item, Item> items;
  public final Map<Item, Item> extension;
  public final ItemSet core;
  private ItemSet baseCore = null;

  public ItemSet(int id) {
    this(id, new HashSet<Item>());
  }

  public ItemSet(int id, Set<Item> items) {
    this.id = id;
    this.items = new HashMap<>();
    for (Item it : items) {
      this.items.put(it, it);
    }
    this.extension = new HashMap<>();
    core = this;
  }

  public ItemSet(int id, ItemSet core, Collection<Item> extension) {
    this.id = id;
    this.items = core.items;
    this.extension = new HashMap<>();
    for (Item it : extension) {
      this.extension.put(it, it);
    }
    this.core = core;
  }

  public int id() {
    return id;
  }

  @Override public String toString() {
    StringBuilder buf = new StringBuilder();
    buf.append(String.format("item set S%d%n", id));
    buf.append(setToString("  ", items));
    buf.append(setToString("+ ", extension));
    return buf.toString();
  }

  private String setToString(String prefix, Map<Item, Item> set) {
    List<String> lines = new ArrayList<>();
    for (Item item : set.values()) {
      lines.add(String.format("%s%s%n", prefix, Util.escape(item.toString())));
    }
    Collections.sort(lines);
    StringBuilder buf = new StringBuilder();
    for (String line : lines) {
      buf.append(line);
    }
    return buf.toString();
  }

  public Map<Symbol, Tuple<Symbol, ItemSet>> followCores() {
    Map<Symbol, Tuple<Symbol, ItemSet>> follows = new HashMap<>();
    for (Item item : items.values()) {
      item.addToFollowSet(follows);
    }
    for (Item item : extension.values()) {
      item.addToFollowSet(follows);
    }
    return follows;
  }

  @Override public int hashCode() {
    // TODO: Cache the hash code (update after change only).
    int hash = 0;
    for (Item item : items.values()) {
      hash ^= item.hashCode();
    }
    return hash;
  }

  @Override public boolean equals(Object obj) {
    if (obj instanceof ItemSet) {
      ItemSet other = (ItemSet) obj;
      return isEqualCollection(items.values(), other.items.values());
    }
    return false;
  }

  public ItemSet core() {
    return core;
  }

  public void printGraphNode() {
    System.out.format("  S%d[label=\"\\N\\n", id());
    printDotSet("  ", items.values());
    printDotSet("+ ", extension.values());
    System.out.println("\"];");
  }

  private static void printDotSet(String prefix, Iterable<Item> set) {
    List<String> items = new ArrayList<>();
    for (Item item : set) {
      items.add(String.format("%s%s\\l", prefix, Util.escape(item.toString())));
    }
    Collections.sort(items);
    for (String item : items) {
      System.out.print(item);
    }
  }

  public ItemSet baseCore() {
    if (baseCore == null) {
      Set<Item> baseItems = new HashSet<>();
      for (Item item : items.values()) {
        baseItems.add(item.baseItem());
      }
      baseCore = new ItemSet(-1, baseItems);
    }
    return baseCore;
  }

  public Collection<Item> relatedItems(Symbol sym) {
    Collection<Item> result = new ArrayList<>();
    List<Item> advanced = new ArrayList<>();
    for (Item item : items.values()) {
      if (item.related(sym)) {
        result.add(item);
      }
    }
    for (Item item : extension.values()) {
      if (item.related(sym)) {
        result.add(item);
      }
    }
    return result;
  }
}
