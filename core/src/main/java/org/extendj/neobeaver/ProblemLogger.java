package org.extendj.neobeaver;

import java.util.ArrayList;
import java.util.List;

public class ProblemLogger implements ProblemHandler {
  private boolean errored = false;
  private List<String> errors = new ArrayList<>();
  private List<String> warnings = new ArrayList<>();

  @Override public void error(String message) {
    errors.add(message);
    errored = true;
  }

  @Override public final void errorf(String fmt, Object... args) {
    error(String.format(fmt, args));
  }

  @Override public void warn(String message) {
    warnings.add(message);
  }

  @Override public final void warnf(String fmt, Object... args) {
    warn(String.format(fmt, args));
  }

  /**
   * Incremental error reporting.
   * Reports all errors that were logged since the last report.
   */
  public void report() {
    for (String error : errors) {
      System.err.print("ERROR: ");
      System.err.println(error);
    }
    for (String warning : warnings) {
      System.err.print("WARNING: ");
      System.err.println(warning);
    }
    errors.clear();
    warnings.clear();
  }

  /** Return {@code true} if there were any errors reported. */
  @Override public boolean errored() {
    return errored;
  }
}
