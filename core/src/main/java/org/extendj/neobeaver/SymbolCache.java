/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.util.HashMap;
import java.util.Map;

public class SymbolCache {
  private int nextId = 2;
  private final Map<String, Symbol> map = new HashMap<>();

  public Symbol nta(String name) {
    return get(new Nonterminal(name));
  }

  public Symbol list(Symbol sym) {
    return get(new Nonterminal("" + sym + "$list"));
  }

  public Symbol opt(Symbol sym) {
    return get(new Nonterminal("" + sym + "$opt"));
  }

  public Symbol terminal(String name) {
    return get(new NamedToken(name));
  }

  public boolean contains(String name) {
    return map.containsKey(name);
  }

  public Symbol get(String name) {
    return map.get(name);
  }

  public Symbol get(Symbol symbol) {
    if (!contains(symbol.name())) {
      map.put(symbol.name(), new IdSymbol(nextId, symbol));
      nextId += 1;
    }
    return get(symbol.name());
  }

}
