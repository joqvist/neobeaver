/* Copyright (c) 2017, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.neobeaver;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.Permission;

import static org.junit.Assert.assertEquals;

public final class TestUtil {
  private TestUtil() { }

  static void runBeaver(Path outputDir, String destDir, String input)
      throws MalformedURLException, ClassNotFoundException, NoSuchMethodException,
      InvocationTargetException, IllegalAccessException {
    URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {
        Paths.get("libs/beaver-cc-0.9.11.jar").toUri().toURL()
    });
    Class<?> beaver = Class.forName("beaver.comp.run.Make", true, classLoader);
    final SecurityManager secMan = System.getSecurityManager();
    try {
      // Redirect output streams.
      PrintStream standardOut = System.out;
      PrintStream standardErr = System.err;
      try (PrintStream out = new PrintStream(new FileOutputStream(
              outputDir.resolve("cc.out").toFile()));
          PrintStream err = new PrintStream(new FileOutputStream(
              outputDir.resolve("cc.err").toFile()))) {
        System.setOut(out);
        System.setErr(err);
        // Prevent Beaver from exiting the VM:
        Method main = beaver.getMethod("main", String[].class);
        System.setSecurityManager(new SecurityManager() {
                                    @Override public void checkPermission(Permission perm) {
            if ((perm instanceof RuntimePermission) && perm.getName().contains("exitVM")) {
              throw new SecurityException("Prevented Beaver from exiting VM.");
            }
            if (secMan != null) {
              secMan.checkPermission(perm);
            }
        }
        });
        main.invoke(null, (Object) new String[] {"-t", "-c", "-d", destDir, input});
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      } finally {
        System.setOut(standardOut);
        System.setErr(standardErr);
      }
    } catch (InvocationTargetException e) {
      if (e.getCause() instanceof SecurityException) {
        assertEquals("Prevented Beaver from exiting VM.", e.getCause().getMessage());
      } else {
        throw e;
      }
    } finally {
      // Restore Security Manager.
      System.setSecurityManager(secMan);
    }
  }

  static void runJFlex(String destDir, String input)
      throws MalformedURLException, ClassNotFoundException, NoSuchMethodException,
      InvocationTargetException, IllegalAccessException {
    URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] {
        Paths.get("libs/jflex-1.6.1.jar").toUri().toURL()
    });
    Class<?> jflex = Class.forName("jflex.Main", true, classLoader);
    Method main = jflex.getMethod("main", String[].class);
    main.invoke(null, (Object) new String[] { "-d", destDir, input });
  }

  static String normalize(String string) {
    return string.trim().replace("\r\n", "\n");
  }

  /**
   * Recursively deletes a directory.
   */
  static void deleteDirectory(Path path) throws IOException {
    Files.walkFileTree(path, new FileVisitor<Path>() {
      @Override public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
          throws IOException {
        return FileVisitResult.CONTINUE;
      }

      @Override public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
          throws IOException {
        Files.delete(file);
        return FileVisitResult.CONTINUE;
      }

      @Override public FileVisitResult visitFileFailed(Path file, IOException exc)
          throws IOException {
        return FileVisitResult.CONTINUE;
      }

      @Override public FileVisitResult postVisitDirectory(Path dir, IOException exc)
          throws IOException {
        if (exc != null) {
          throw exc;
        }
        Files.delete(dir);
        return FileVisitResult.CONTINUE;
      }
    });
  }
}
