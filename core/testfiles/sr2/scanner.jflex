package org.extendj.parsertest;

import static org.extendj.parsertest.Parser.Terminals.*;

import org.extendj.parsertest.Parser.Token;
import org.extendj.parsertest.Parser.Terminals;
import org.extendj.parsertest.Parser.SourcePosition;

import java.io.IOException;
%%

%public
%final
%class Scanner
%extends beaver.Scanner

%type beaver.Symbol 
%function nextToken
%yylexthrow IOException

%unicode
%line %column

%{
  private Token sym(int id) {
    return new Token(id, yytext(), pos());
  }

  private SourcePosition pos() {
    return new SourcePosition(yyline + 1, yycolumn + 1);
  }

  private void error(String msg) throws IOException {
    throw new IOException(
        String.format("%d:%d: %s", yyline + 1, yycolumn + 1, msg));
  }
%}

WhiteSpace = [ ] | \t | \f | \n | \r
id = [_a-zA-Z][_a-zA-Z0-9]*

%%

<YYINITIAL> {
  {WhiteSpace} { }
  "if"         { return sym(Terminals.IF); }
  "return"     { return sym(Terminals.RETURN); }

  "("          { return sym(Terminals.LPAREN); }
  ")"          { return sym(Terminals.RPAREN); }

  "{"          { return sym(Terminals.LBRACE); }
  "}"          { return sym(Terminals.RBRACE); }

  ";"          { return sym(Terminals.SEMICOLON); }
  ","          { return sym(Terminals.COMMA); }

  {id}         { return sym(Terminals.ID); }
}

/* Fall through errors. */
[^] {
  error("illegal character \"" + yytext() +  "\"");
}

<<EOF>> {
  return new Token(EOF, "EOF", pos());
}
