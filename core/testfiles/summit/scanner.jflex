package org.extendj.parsertest;

import static org.extendj.parsertest.Parser.Terminals.*;

import org.extendj.parsertest.Parser.Token;
import org.extendj.parsertest.Parser.Terminals;
import org.extendj.parsertest.Parser.SourcePosition;

import java.io.IOException;
%%

%public
%final
%class Scanner
%extends beaver.Scanner

%type beaver.Symbol
%function nextToken
%yylexthrow IOException

%unicode
%line %column

%{
  private Token sym(short id) {
    return new Token(id, yytext(), pos());
  }

  private SourcePosition pos() {
    return new SourcePosition(yyline + 1, yycolumn + 1);
  }

  int stringStartLine, stringStartColumn;
  StringBuilder stringBuilder = new StringBuilder();
  int commentDepth = 0;

  private void startString() {
    yybegin(STRING);
    stringStartLine = yyline + 1;
    stringStartColumn = yycolumn + 1;
    stringBuilder.setLength(0);
  }

  private Token stringEnd() {
    yybegin(YYINITIAL);
    String literal = stringBuilder.toString();
    int length = literal.length() + 2;
    return new Token(Terminals.STRING, literal, pos());
  }

  private void error(String msg) throws IOException {
    throw new IOException(
        String.format("%d:%d: %s", yyline + 1, yycolumn + 1, msg));
  }

  private void enterComment() {
    if (commentDepth == 0) {
      yybegin(COMMENT);
    }
    commentDepth += 1;
  }

  private void leaveComment() {
    commentDepth -= 1;
    if (commentDepth == 0) {
      yybegin(YYINITIAL);
    }
  }
%}

eol = \n|\r
input_char = [^\r\n]

whitespace = [ ] | \t | \f | {eol}

eol_comment = "//" {input_char}* {eol}?

digit = [0-9]
num = {digit}+
id = [_a-zA-Z][_a-zA-Z0-9]*

string_char = [^\r\n\"]

line_terminator = \n|\r|\r\n

%state STRING
%state COMMENT

%%

<YYINITIAL> {

  {whitespace} { }
  {eol_comment} { }

  "if"         { return sym(Terminals.IF); }
  "else"       { return sym(Terminals.ELSE); }
  "while"      { return sym(Terminals.WHILE); }
  "return"     { return sym(Terminals.RETURN); }
  "void"       { return sym(Terminals.VOID); }
  "int"        { return sym(Terminals.INT); }
  "byte"       { return sym(Terminals.BYTE); }
  "bool"       { return sym(Terminals.BOOL); }
  "true"       { return sym(Terminals.TRUE); }
  "false"      { return sym(Terminals.FALSE); }
  "struct"     { return sym(Terminals.STRUCT); }
  "enum"       { return sym(Terminals.ENUM); }
  "typename"   { return sym(Terminals.TYPENAME); }

  "@"          { return sym(Terminals.AT); }
  "->"         { return sym(Terminals.ARROW); }
  "="          { return sym(Terminals.EQ); }
  "<"          { return sym(Terminals.LT); }
  ">"          { return sym(Terminals.GT); }
  "=="         { return sym(Terminals.EQEQ); }
  "<="         { return sym(Terminals.LTEQ); }
  ">="         { return sym(Terminals.GTEQ); }
  "!="         { return sym(Terminals.NOTEQ); }
  "+"          { return sym(Terminals.ADD); }
  "-"          { return sym(Terminals.SUB); }
  "*"          { return sym(Terminals.MUL); }
  "/"          { return sym(Terminals.DIV); }
  "%"          { return sym(Terminals.MOD); }
  "&&"         { return sym(Terminals.AND); }
  "||"         { return sym(Terminals.OR); }
  "^"          { return sym(Terminals.XOR); }
  "!"          { return sym(Terminals.NOT); }

  // "."          { return sym(Terminals.DOT); }
  "..."        { return sym(Terminals.ELLIPSIS); }

  "["          { return sym(Terminals.LBRACKET); }
  "]"          { return sym(Terminals.RBRACKET); }

  "("          { return sym(Terminals.LPAREN); }
  ")"          { return sym(Terminals.RPAREN); }

  "{"          { return sym(Terminals.LBRACE); }
  "}"          { return sym(Terminals.RBRACE); }

  ";"          { return sym(Terminals.SEMICOLON); }
  ":"          { return sym(Terminals.COLON); }
  ","          { return sym(Terminals.COMMA); }

  "/*"         { enterComment(); }

  \"           { startString(); }

  {id}         { return sym(Terminals.ID); }

  {num}        { return sym(Terminals.NUM); }
}

<COMMENT> {
  "/*"         { enterComment(); }
  "*/"         { leaveComment(); }
  [^]          { }
}

<STRING> {
  \"                { return stringEnd(); }
  {string_char}+    { stringBuilder.append(yytext()); }
  {line_terminator} { error("unterminated string at end of line"); }
}

[^]            { error("Illegal character <" + yytext() + ">"); }
<<EOF>>        { return sym(Terminals.EOF); }
