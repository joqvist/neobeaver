plugins {
  id 'java'
  id 'application'
  id 'maven-publish'
  id 'signing'
  id 'org.jastadd' version '1.14.5'
}

repositories {
  mavenCentral()
  mavenLocal()
}

sourceSets {
  main.java.srcDir 'src/gen'
}

configurations {
  jflex
}

dependencies {
  implementation 'com.google.guava:guava:20.0'
  implementation 'org.extendj:trace:0.1'
  implementation 'se.llbit:collcompare:1.0.1'

  testImplementation 'junit:junit:4.12'

  jflex 'de.jflex:jflex:1.6.1'
}

sourceCompatibility = targetCompatibility = '1.7'

def mainClassName = 'org.extendj.neobeaver.NeoBeaver'
application {
  mainClass = mainClassName
}

archivesBaseName = 'neobeaver'

jar {
  manifest.attributes 'Main-Class': mainClassName
}

test.dependsOn 'cleanTest'
compileJava.dependsOn 'generateScanner'
compileJava.dependsOn 'generateAst'

task generateScanner(type: JavaExec) {
  classpath = configurations.jflex
  mainClass = 'jflex.Main'
  args '-d', file('src/gen/org/extendj/neobeaver').path,
      file('src/scanner/Scanner.jflex').path

  inputs.file file('src/scanner/Scanner.jflex')
  outputs.file file('src/gen/org/extendj/neobeaver/Scanner.java')

  doFirst {
    file('src/gen/org/extendj/neobeaver').mkdirs()
  }
}

task generateAst(type: org.jastadd.JastAddTask) {
  outputDir = file('src/gen/')
  sources = fileTree('src/grammar')
  options = [ '--package=org.extendj.neobeaver.ast' ]
}

clean.dependsOn 'cleanGen'

task('cleanGen', type: Delete) {
  description 'Removes generated files.'
  delete file('src/gen')
}

task fatJar(type: Jar) {
  destinationDirectory = rootProject.projectDir

  manifest.attributes 'Main-Class': mainClassName

  from { configurations.runtimeClasspath.collect { it.isDirectory() ? it : zipTree(it) } }
  with jar
}

javadoc {
  failOnError = false
}

java {
  withJavadocJar()
  withSourcesJar()
  sourcesJar.dependsOn 'generateAst'
  sourcesJar.dependsOn 'generateScanner'
}

publishing {
  publications {
    mavenJava(MavenPublication) {
      artifactId = 'neobeaver'
      from components.java

      versionMapping {
        usage('java-api') {
          fromResolutionOf('runtimeClasspath')
        }
        usage('java-runtime') {
          fromResolutionResult()
        }
      }

      pom {
        name = 'NeoBeaver'
        description = 'LALR parser generator replacement for Beaver.'
        url = 'https://extendj.org/neobeaver.html'
        licenses {
          license {
            name = 'Modified BSD License'
            url = 'http://opensource.org/licenses/BSD-3-Clause'
            distribution = 'repo'
          }
        }
        developers {
          developer {
            name = 'Jesper Öqvist'
            email = 'jesper.oqvist@cs.lth.se'
          }
        }
        scm {
          connection = 'scm:git:https://bitbucket.org/joqvist/neobeaver.git'
          url = 'https://bitbucket.org/joqvist/neobeaver'
        }
      }
    }
  }

  if (project.hasProperty('ossrhUsername')) {
    repositories {
      maven {
        url = 'https://oss.sonatype.org/service/local/staging/deploy/maven2'
        credentials {
          username = ossrhUsername
          password = ossrhPassword
        }
      }
    }
  }

  signing {
    // Require keyId to sign:
    required { project.hasProperty('signing.keyId') }
    sign publishing.publications.mavenJava
  }
}

