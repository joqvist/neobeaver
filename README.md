# NeoBeaver

This is a replacement for the [Beaver][1] parser generator with the
goal of being easier to use.

Things that NeoBeaver does differently:

* Improved warning & error messages.
* Unused terminals are not removed.
* Implicit terminals.
* Inline type declarations.
* Automatic component names.
* Directives anywhere.


More deails about each of the differences are described below.

## Improved warning & error messages

If there is a conflict, NeoBeaver prints the parsing states for each conflict.

For example,

    WARNING: resolved SHIFT/REDUCE conflict on [MUL] by selecting SHIFT:
      REDUCE exp = exp MUL exp
      SHIFT MUL
    Context:
      exp = exp . MUL exp [END]
      exp = exp MUL exp . [MUL]
      exp = exp . MUL exp [EOF]
      exp = exp . MUL exp [MUL]
      exp = exp . MUL exp [RBRACKET]
      exp = exp . MUL exp [IN]
      exp = exp . MUL exp [ID]


## Unused terminals are not removed

Although Beaver requires explicit terminal declarations, it will remove
terminals that are not used anywhere in the parsing grammar.  Thus, the
terminal will not appear in the terminal definitions and cannot be used by a
scanner.  This can be annoying when developing a new parser, as it does not
allow a partially completed parser specification to compile together with a
completed scanner.

NeoBeaver instead always generates terminals that have been explicitly
declared, making it easier to incrementally develop a new parser: you can
specify all terminals and make your scanner compile without completing the
parser first.

    %terminals ADD, MUL;
    %terminals LPAREN, RPAREN;
    %terminals ID;


## Implicit terminals

NeoBeaver does not require all terminals to be explicitly declared. Any symbol
name which does not occur as the left-hand side of a production is assumed to
be a terminal name. However, a warning is reported for each such implicitly
declared terminal.

For example, in the grammar

    %terminals ID;
    decl = ID EQ exp;
    exp = ...;


EQ is treated as an implicitly declared terminal and a warning is printed:

    WARNING: 2:11: implicit declaration of terminal symbol [EQ].


Beaver reports an error for the above grammar.

## Inline type declarations

In Beaver, you have to specify the type of each production by using `%typeof` declarations.
This can be replaced by using inline type specifiers in NeoBeaver. For example,

    MyType product = comp1 comp2;


## Automatic component names

When you want to use a production component in the semantic action, you have to give it a
name in Beaver. For example,


    sender = name.name address.addr {: return new Sender(name, addr); };


If no name is given, NeoBeaver uses the components name instead:

    sender = name address {: return new Sender(name, address); };


If there are is a repetition of a component then the repeated components will have
names suffixed with their repetition count (n, n2, n3, etc.).


## Directives anywhere

Unlike Beaver, NeoBeaver allows parser gnerator directives such as `%left` and
`%terminals` to be mixed with grammar productions.

## How to run it

NeoBeaver can generate a standalone parser that does not require the Beaver runtime
classes, or it can be used in Beaver compatibility mode generating a parser
that can be dropped in as a replacement of a Beaver-generated parser.

Use the following commands to generate a classic Beaver parser using NeoBeaver:

    ./gradlew fatJar
    java -jar neobeaver-0.1.6.jar --beaver <grammar>


For a standalone parser use the `--standalone` option instead of `--beaver`.

## Beaver Frontend

There is a frontend with the same interface as classic Beaver. The frontend uses
the Maven id `nbfront`.

## License

Copyright (c) 2017, Jesper Öqvist

This project is provided under the Modified BSD License.
See the LICENSE file, in the same directory as this README file.


[1]: http://beaver.sourceforge.net/
